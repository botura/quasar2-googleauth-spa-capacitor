# Quasar + GoogleAuth + SPA + Capacitor


Follow usefull link (1) below just to configure your firebase project, google api, activate Google sign in, get firebase webClientId (firebase android app), get firebase apiKey & authDomain (firebase web app) and your local SHA1 key.

Then, just change these three files in your Quasar code (no other file needs to be changed or downloaded from google - don´t follow uselfull links for this):

1) `src/boot/auth.js`: insert your firebase apiKey and authDomain (from Firebase Console / Web App).
2) `/src/pages/auth/components/login.vue`: insert your webClientId (from Google Api Console / Credentials)
3) `/src/pages/auth/components/register.vue`: same as above

Install capacitor mode ("Capacitor app id" is the same used in firebase configuration, see usefull links below):

```
$ quasar mode add capacitor
```

Install google-plus plugin and test:

```
$ cd src-capacitor
$ npm install cordova-plugin-googleplus
$ npm install @ionic-native/google-plus
$ quasar dev -m capacitor -T android
```

Usefull links:

- (1) https://devdactic.com/capacitor-google-sign-in/
- (2) https://ionicframework.com/docs/native/google-plus
- (3) https://github.com/EddyVerbruggen/cordova-plugin-googleplus
- (4) https://firebase.google.com/docs/auth/web/google-signin
