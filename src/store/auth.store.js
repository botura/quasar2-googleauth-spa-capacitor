import { firebaseAuth, provider } from "src/boot/auth";
import { Loading } from "quasar";
import _, { isNull } from "lodash";
import firebase from "firebase/app";
import { notifyError, notifyInfo } from "src/functions/messages";

const state = {
  loggedIn: false,
  user: {},
  credential: {},
  token: "",
  roles: [],
};

const mutations = {
  SET_LOGGED_IN(state, value) {
    state.loggedIn = value;
  },

  SET_USER(state, value) {
    state.user = value;
  },

  SET_CREDENTIAL(state, value) {
    state.credential = value;
  },

  SET_TOKEN(state, value) {
    state.token = value;
  },

  SET_ROLES(state, value) {
    state.roles = value;
  },
};

const actions = {
  loginUser({ commit }) {
    // firebaseAuth.signInWithRedirect(provider);
    firebaseAuth
      .signInWithPopup(provider)
      .then((result) => {
        // This gives you a Google Access Token. You can use it to access the Google API.
        // console.log(result.credential);
        // TODO: Precisa da credencial?
        commit("SET_CREDENTIAL", result.credential);
        // The signed-in user info.
        // var user = result.user;
        // console.log(user);
      })
      .catch((error) => {
        console.log(error.message);
      });
  },

  logoutUser() {
    firebaseAuth.signOut();
  },

  handleAuthStateChange({ commit, dispatch }) {
    // console.log("handleAuthStateChange");
    Loading.show();
    firebaseAuth.onIdTokenChanged((user) => {
      // console.log("user: ", user);
      if (user) {
        let aux = user.displayName ? user.displayName : user.email;
        if (!aux) aux = "";
        commit("SET_USER", {
          displayName: aux,
          initialString: aux.substring(0, 2).toUpperCase(),
          email: user.email,
          photoURL: user.photoURL,
        });
        commit("SET_LOGGED_IN", true);
        firebaseAuth.currentUser.getIdTokenResult().then((idTokenResult) => {
          // console.log("idTokenResult: ", idTokenResult);
          commit("SET_TOKEN", idTokenResult.token);
          commit("SET_ROLES", idTokenResult.claims.roles);
        });
        this.$router.push("/").catch((err) => {});
        Loading.hide();
      } else {
        commit("SET_USER", {});
        commit("SET_LOGGED_IN", false);
        commit("SET_TOKEN", "");
        commit("SET_ROLES", {});
        this.$router.replace("/").catch((err) => {});
        Loading.hide();
      }
    });
  },

  // Sign in with credential from the Google user.
  signInWithCredential({ commit, dispatch }, idToken) {
    console.log("idToken", idToken);
    var credential = firebase.auth.GoogleAuthProvider.credential(idToken);

    firebase
      .auth()
      .signInWithCredential(credential)
      .catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
        notifyError(error.message);
      });
  },

  signInWithEmailAndPassword({ commit, dispatch }, { email, password }) {
    firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .then((user) => {
        // Signed in
        // ...
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        notifyError(error.message);

        // ..
      });
  },

  createUserWithEmailAndPassword({ commit, dispatch }, { email, password }) {
    firebaseAuth
      .createUserWithEmailAndPassword(email, password)
      .then((user) => {
        // Signed in
        // ...
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        notifyError(error.message);
      });
  },

  sendPasswordReset({ commit, dispatch }, email) {
    console.log(email);
    firebaseAuth
      .sendPasswordResetEmail(email)
      .then(() => {
        // Password reset email sent!
        notifyInfo(
          "Um email foi enviado, verifique sua caixa de entrada e siga as instruções para definir uma nova senha."
        );
        // ..
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
        notifyError(error.message);
      });
  },
};

const getters = {
  hasRole: (state) => (role) => {
    return state.roles ? state.roles.find((r) => r == role) : false;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
