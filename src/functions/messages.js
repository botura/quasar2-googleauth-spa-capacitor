import { Notify } from "quasar";

export function notifyError(message) {
  Notify.create({
    message: "Alguma coisa deu errado!",
    caption: message,
    type: "negative",
    progress: true,
    multiLine: true,
    position: "center",
    timeout: 5000,
    actions: [
      {
        label: "Fechar",
        color: "yellow",
        handler: () => {
          // console.log('dismissed');
        },
      },
    ],
  });
}

export function notifyInfo(message) {
  Notify.create({
    message: message,
    caption: null,
    color: "primary",
    progress: true,
    multiLine: true,
    position: "center",
    timeout: 5000,
    actions: [
      {
        label: "Fechar",
        color: "white",
        handler: () => {
          // console.log('dismissed');
        },
      },
    ],
  });
}
