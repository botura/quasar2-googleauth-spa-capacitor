// import something here
// import Vue from "vue";
import { boot } from "quasar/wrappers";
import firebase from "firebase/app";

import "firebase/auth";

// ADD YOUR FIREBASE CONFIG OBJECT HERE:
var firebaseConfig = {
  apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  authDomain: "XXXXXXXXXXXXX.firebaseapp.com",
};

let firebaseApp = firebase.initializeApp(firebaseConfig);
let firebaseAuth = firebaseApp.auth();

var provider = new firebase.auth.GoogleAuthProvider();

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
// export default async (/* { app, router, Vue ... } */) => {
//   // something to do
// };

// Vue.prototype.$auth = firebaseAuth;

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$auth = firebaseAuth;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file
});

// TODO: Precisa dessa rotina?
// async function getClaims() {
//   var claims = {};
//   await firebaseAuth.currentUser.getIdTokenResult().then(idTokenResult => {
//     claims = idTokenResult.claims.permissoes;
//   });
//   return claims;
// }

export { firebaseAuth, provider /*, getClaims */ };
